#include "blk_imwrite.h"

blk_imwrite::blk_imwrite() : blk("blk_imwrite") {
}

void blk_imwrite::process_one_task(const blk_payload in_payload) {
  if(d_debug) {
    std::clog << blk_name() << "::process_one_task()" << std::endl;
  }
  std::size_t slash_loc = in_payload.d_string.rfind('/');
  std::string infn = in_payload.d_string.substr(slash_loc + 1);
  std::string outfn = d_conf_string.at("dst_path") + infn + ".ppm";

  if(in_payload.m_string.at("dtype") == "uint8") {
    write_as_pgm(in_payload.v_uint8,
                 in_payload.m_uint32.at("width"),
                 in_payload.m_uint32.at("height"),
                 outfn);
  } else {
    std::vector<uint8_t> rgb(in_payload.v_float.size());
    for(uint32_t i=0; i<rgb.size(); ++i) {
      float val = in_payload.v_float[i] * 255.5f;
      rgb[i] = (uint8_t)(fmin(val, 255.0f));
    }
    write_as_pgm(rgb,
                 in_payload.m_uint32.at("width"),
                 in_payload.m_uint32.at("height"),
                 outfn);
  }

  blk_payload out_payload(in_payload);
  add_one_task(out_payload);
}

void blk_imwrite::write_as_pgm(const std::vector<uint8_t> &rgb, uint32_t width, uint32_t height, std::string fn) {
  std::ofstream f(fn);
  if(f.is_open()) {
    f << "P3\n" << width << " " << height << "\n255\n";
    for(uint32_t i=0;i <rgb.size(); i=i+3) {
      f << (int) rgb[i] << " " << (int) rgb[i+1] << " " << (int) rgb[i+2] << std::endl;
    }
    f.close();
    std::clog << "written image to " << fn << std::endl;
  } else {
    std::cerr << "failed to write image to file: "
              << fn << std::endl;
  }
}
