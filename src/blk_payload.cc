#include "blk_payload.h"

blk_payload::blk_payload() {
}

blk_payload::blk_payload(const blk_payload &payload) {
  m_uint32 = std::unordered_map<std::string, uint32_t>(payload.m_uint32);
  m_string = std::unordered_map<std::string, std::string>(payload.m_string);
  d_string = payload.d_string;
  d_uint32 = payload.d_uint32;
  d_uint8  = payload.d_uint8;

  v_uint32 = std::vector<uint32_t>(payload.v_uint32);
  v_uint8  = std::vector<uint8_t>(payload.v_uint8);
  v_float  = std::vector<float>(payload.v_float);
  v_double  = std::vector<double>(payload.v_double);
  v_string  = std::vector<std::string>(payload.v_string);
}
