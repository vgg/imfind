#ifndef BLK_IMREAD_H
#define BLK_IMREAD_H

// loads image data corresponding to an image filename
//
// Author: Abhishek Dutta <adutta@robots.ox.ac.uk>
// Date: 4 April 2020

#include "blk_payload.h"
#include "blk.h"

#include <iostream>
#include <string>

#include "tensorflow/lite/builtin_op_data.h"
#include "tensorflow/lite/interpreter.h"
#include "tensorflow/lite/kernels/register.h"
#include "tensorflow/lite/string_util.h"

#ifndef STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#define STBI_NO_PSD
#define STBI_NO_TGA
#define STBI_NO_GIF
#define STBI_NO_HDR
#define STBI_NO_PIC
#define STB_IMAGE_STATIC
#include "stb_image.h"
#endif

class blk_imread : public blk {
 public:
  blk_imread();

 private:
  void process_one_task(const blk_payload in_payload) override;
  void skip_one_task(const blk_payload &in_payload, std::string reason) const override;

  // not used currently
  bool tflite_imresize(unsigned char *rgb,
                       int image_width, int image_height, int image_channels,
                       int wanted_width, int wanted_height, int wanted_channels,
                       blk_payload &out_payload);
  bool stb_imresize(unsigned char *rgb,
                    int image_width, int image_height, int image_channels,
                    int wanted_width, int wanted_height, int wanted_channels,
                    blk_payload &out_payload);
};

#endif
