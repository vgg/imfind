/*
** a minimal logging system used by imfind
**
** Author: Abhishek Dutta <adutta _at_ robots.ox.ac.uk>
** Date: Feb. 15, 2020
**
*/

#ifndef IMFIND_LOG_H
#define IMFIND_LOG_H

namespace imfind {
  class log {
  public:
    log(std::string log_fn, bool append=true) : d_log_fn(log_fn) {
      if(append) {
        d_logf.open(d_log_fn, std::ofstream::app);
      } else {
        d_logf.open(d_log_fn, std::ofstream::trunc);
      }
      if(!d_logf) {
        std::cerr << "failed to initialize logger: "
                  << d_log_fn << std::endl;
      }
      std::time_t current_time = std::time(nullptr);
      std::string current_time_str(std::asctime(std::localtime(&current_time)));
      current_time_str = current_time_str.substr(0, current_time_str.length() - 1); // remove trailing \n
      d_logf << "--------[ Logging started on "
             << current_time_str << " ]--------"
             << std::endl;
    }

    void log_cmd(const int argc, const char **argv) {
      for(int i=0; i < argc; ++i) {
        d_logf << argv[i] << " ";
      }
      d_logf << std::endl;
    }

    void src(std::ostream *s) {
      d_src = s;
      d_src_old_rdbuf = d_src->rdbuf();
      d_src->rdbuf(d_logf.rdbuf());
    }

    ~log() {
      if(d_logf) {
        std::time_t current_time = std::time(nullptr);
        std::string current_time_str(std::asctime(std::localtime(&current_time)));
        current_time_str = current_time_str.substr(0, current_time_str.length() - 1); // remove trailing \n
        d_logf << "[ Logging ended on "
               << current_time_str << " ]" << std::endl;
        d_logf.close();
      }
      if(d_src) {
        d_src->rdbuf(d_src_old_rdbuf);
      }
    }

  private:
    std::ofstream d_logf;
    std::string d_log_fn;
    std::streambuf *d_src_old_rdbuf;
    std::ostream *d_src;
  };
}
#endif
