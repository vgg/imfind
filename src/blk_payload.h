#ifndef BLK_PAYLOAD_H
#define BLK_PAYLOAD_H

#include <string>
#include <vector>
#include <unordered_map>

class blk_payload {
 public:
  blk_payload();
  blk_payload(const blk_payload &payload);

  // NOTE: if you add a new member variable, you must
  // also update the copy constructor defined in blk_payload.cc

  // map (prefixed with m_)
  std::unordered_map<std::string, uint32_t> m_uint32;
  std::unordered_map<std::string, std::string> m_string;

  // fundamental data type (prefixed with d_)
  std::string d_string;
  uint32_t d_uint32;
  uint8_t d_uint8;

  // vector (prefixed with v_)
  std::vector<uint32_t> v_uint32;
  std::vector<uint8_t> v_uint8;
  std::vector<float> v_float;
  std::vector<double> v_double;
  std::vector<std::string> v_string;
};
#endif
