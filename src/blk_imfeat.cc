#include "blk_imfeat.h"

blk_imfeat::blk_imfeat() : blk("blk_imfeat") {
}

void blk_imfeat::get_ready_for_inference() {
  load_model();
}

void blk_imfeat::load_model() {
  std::string tflite_model_fn = d_conf_string["tflite_model_fn"];

  d_tflite_model = tflite::FlatBufferModel::BuildFromFile(tflite_model_fn.c_str());
  if(d_tflite_model == nullptr) {
    std::cerr << blk_name() << ": failed to load model: "
              << tflite_model_fn << std::endl;
    return;
  }

  tflite::ops::builtin::BuiltinOpResolver resolver;
  tflite::InterpreterBuilder builder(*d_tflite_model, resolver);
  builder(&d_interpreter);
  if(d_interpreter == nullptr) {
    std::cerr << "interpreter initialization failed" << std::endl;
    return;
  }

  if(d_interpreter->AllocateTensors() != kTfLiteOk) {
    std::cerr << "interpreter->AllocateTensors() failed" << std::endl;
    return;
  }
  d_interpreter->SetNumThreads(d_conf_uint32.at("tflite_interpreter_num_threads")); // to ensure each worker is single threaded
  //std::cout << blk_name() << ": using " << d_conf_uint32.at("tflite_interpreter_num_threads") << " threads" << std::endl;
}

void blk_imfeat::process_one_task(const blk_payload in_payload) {
  blk_payload out_payload;
  out_payload.d_string = in_payload.d_string;
  out_payload.m_string["src_path"] = in_payload.m_string.at("src_path");

  /*
  // memcpy does not work or has issues sometimes
  // I do not understand this behaviour yet!
  memcpy(d_interpreter->typed_input_tensor<float>(0),
         in_payload.v_float.data(),
         in_payload.v_float.size());
  */
  for(std::size_t i=0; i<in_payload.v_float.size(); ++i) {
    d_interpreter->typed_input_tensor<float>(0)[i] = in_payload.v_float.at(i);
  }

  /*
  std::cout << blk_name() << ": " << out_payload.d_string << ": after memcpy"
            << ", showing vertical center pixel values:"
            << in_payload.v_float.size() << std::endl;
  uint32_t w = in_payload.m_uint32.at("width");
  uint32_t h = in_payload.m_uint32.at("height");
  uint32_t c = in_payload.m_uint32.at("nchannel");
  unsigned int center = w/2;
  for(unsigned int i=0; i<h; ++i) {
    std::cout << std::fixed << std::setprecision(2)
              << "(" << i << ":"
              << d_interpreter->typed_input_tensor<float>(0)[i*w*c + center] << ","
              << d_interpreter->typed_input_tensor<float>(0)[i*w*c + center + 1] << ","
              << d_interpreter->typed_input_tensor<float>(0)[i*w*c + center + 2] << "), ";

  }
  std::cout << std::endl;
  */

  if(d_interpreter->Invoke() != kTfLiteOk) {
    std::cerr << blk_name() << "failed to run model forward pass"
              << std::endl;
    return;
  }

  TfLiteTensor *output = d_interpreter->tensor(d_conf_uint32.at("tflite_model_output_index"));
  uint32_t output_size = 1;
  for(uint32_t i=0; i<output->dims->size; ++i) {
    output_size = output_size * output->dims->data[i];
  }

  if(d_conf_string.at("tflite_model_output_apply") == "AveragePool2D") {
    // apply average pooling to 10x10x272 feature map to
    // see https://github.com/tensorflow/tensorflow/issues/36226#issuecomment-579421260
    uint32_t feature_dim = output->dims->data[ output->dims->size - 1 ];
    out_payload.v_float.assign(feature_dim, 0.0);
    for(uint32_t i=0; i<output_size; ++i) {
      out_payload.v_float[ i % feature_dim ] += output->data.f[i];
    }
    float inv_filter_count = ((float) feature_dim) / ((float) output_size);

    double sq_sum = 0.0;
    for(uint32_t i=0; i<feature_dim; ++i) {
      out_payload.v_float[i] = out_payload.v_float[i] * inv_filter_count;
      sq_sum += out_payload.v_float[i] * out_payload.v_float[i];
    }

    // normalize feature vector
    float norm_const = (float) (1.0 / sqrt(sq_sum));
    for(uint32_t i=0; i<feature_dim; ++i) {
      out_payload.v_float[i] = out_payload.v_float[i] * norm_const;
    }
    out_payload.m_uint32["feature_dimension"] = feature_dim;
  } else {
    uint32_t feature_dim = output_size;
    out_payload.v_float.assign(feature_dim, 0.0);
    for(uint32_t i=0; i<output_size; ++i) {
      out_payload.v_float[i] = output->data.f[i];
    }
  }

  /*
  std::cout << "blk_imfeat::process_one_task() for "
            << out_payload.d_string << "=[";
  for(unsigned int i=0; i<out_payload.v_float.size(); ++i) {
    std::cout << out_payload.v_float[i] << ", ";
  }
  std::cout << "]" << std::endl;
  */
  add_one_task(out_payload);
}
