#ifndef BLK_IMWRITE_H
#define BLK_IMWRITE_H

// write image data to file
//
// Author: Abhishek Dutta <adutta@robots.ox.ac.uk>
// Date: 7 April 2020

#include "blk_payload.h"
#include "blk.h"

#include <iostream>
#include <fstream>
#include <cmath>

class blk_imwrite : public blk {
public:
  blk_imwrite();
private:
  void process_one_task(const blk_payload in_payload);
  void write_as_pgm(const std::vector<uint8_t> &rgb, uint32_t width, uint32_t height, std::string fn);
};

#endif
