#ifndef BLK_FLIST_H
#define BLK_FLIST_H

// generates a list of image filenames contained in a folder
//
// Author: Abhishek Dutta <adutta@robots.ox.ac.uk>
// Date: 4 April 2020

#include "blk.h"

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <dirent.h>
#include <regex>

#include <future>
#include <iostream>

class blk_flist : public blk {
 public:
  blk_flist();

  std::string src_path();
  void src_path(std::string src_path);
  uint32_t file_count();
 private:
  std::string d_src_path;
  std::future<uint32_t> d_file_add_count;

  uint32_t ls_and_queue(std::string dirpath,
                    std::string filename_prefix);
  bool fs_is_dir(std::string p);
  void process_one_task(const blk_payload in_payload) override;
};
#endif
