#ifndef BLK_FEAT_INDEX_H
#define BLK_FEAT_INDEX_H

// nearest neighbour index of image features
//
// Author: Abhishek Dutta <adutta@robots.ox.ac.uk>
// Date: 9 April 2020

#include "blk_payload.h"
#include "blk.h"

#include <iostream>
#include <functional>
#include <map>

#include <faiss/IndexFlat.h>
#include <faiss/IndexIVFFlat.h>
#include <faiss/IndexBinaryFlat.h>
#include <faiss/IndexIVFPQ.h>
#include <faiss/IndexHNSW.h>
#include <faiss/VectorTransform.h>
#include <faiss/IndexPreTransform.h>
#include <faiss/index_io.h>
#include <faiss/impl/AuxIndexStructures.h>
#include <faiss/utils/utils.h> // for faiss::getmillisecs()

class blk_feat_index : public blk {
 public:
  blk_feat_index();
  void get_ready_for_indexing();
 private:
  void process_one_task(const blk_payload in_payload) override;
  void on_all_done();

  faiss::IndexFlatL2 d_index;
  std::string d_index_fn;
  std::vector<std::string> d_filename;
  std::vector<float> d_feature;
};

#endif
