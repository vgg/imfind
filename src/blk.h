#ifndef BLK_H
#define BLK_H

// defines the interface that must be available in each block
//
// Author: Abhishek Dutta <adutta@robots.ox.ac.uk>
// Date: 4 April 2020

#include "blk_payload.h"

#include <iostream>
#include <queue>
#include <condition_variable>
#include <string>
#include <future>
#include <stack>
#include <chrono>

class blk {

public:
  blk(std::string blk_name);
  ~blk();

  virtual void in(blk* in, bool wait_until_finish=false);
  virtual blk* in();

  virtual bool get_one_task(blk_payload &payload,
                            const std::string caller_blk_name); // caller_blk_name used for debug
  virtual void add_one_task(blk_payload payload);
  virtual void process_one_task(const blk_payload in_payload);
  virtual void skip_one_task(const blk_payload &in_payload, std::string reason) const;
  virtual bool has_task() const;
  virtual void on_all_done();

  void output_buffer_size(uint32_t buffer_size);
  uint32_t output_buffer_size();

  std::string blk_name() const;
  void print_flow_graph() const;

  std::unordered_map<std::string, std::string> d_conf_string;
  std::unordered_map<std::string, uint32_t> d_conf_uint32;
  std::unordered_map<std::string, float> d_conf_float;
  std::unordered_map<std::string, double> d_conf_double;
  std::unordered_map<std::string, bool> d_conf_bool;

  bool d_debug;
protected:
  std::string d_blk_name;
  bool d_has_task;
  std::chrono::system_clock::time_point d_start_time;
  std::chrono::system_clock::time_point d_end_time;
private:
  std::queue< std::shared_ptr<blk_payload> > d_task_queue;
  mutable std::mutex d_get_one_task_mutex;
  mutable std::mutex d_add_one_task_mutex;
  std::condition_variable d_get_task_condition;
  std::condition_variable d_add_task_condition;

  std::future<uint32_t> d_processed_payload_count;

  blk *d_in;
  uint32_t on_in();

  uint32_t d_output_buffer_size;
  void process_one_payload();
  void print_task_queue() const;
};

#endif
