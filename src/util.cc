#include "util.h"

void imfind::parse_cmdline_args(const int argc, const char **argv,
                                std::map<std::string, std::string> &cmdline_args) {
  cmdline_args.clear();
  if(argc<2) {
    return;
  }

  uint32_t arg_index_start = 1;      // ignore first argument i.e. imfind
  uint32_t arg_index_end = argc - 1; // ignore last argument i.e. input folder
  uint32_t arg_index = arg_index_start;
  while(arg_index < arg_index_end) {
    std::string arg(argv[arg_index]);
    if(arg.length() < 2) {
      arg_index = arg_index + 1;
    } else {
      if(arg[0] == '-' && arg[1] == '-') {
        std::string key = arg;
        std::string val;
        if( (arg_index+1) < arg_index_end ) {
          std::string next_arg(argv[arg_index+1]);
          if((next_arg[0] == '-' &&
              next_arg[1] == '-') ||
             next_arg.find('=') != std::string::npos ) {
            val = "";
            arg_index = arg_index + 1;
          } else {
            val = argv[arg_index + 1];
            arg_index = arg_index + 2;
          }
        } else {
          arg_index = arg_index + 1;
        }
        cmdline_args.insert(std::pair<std::string, std::string>(key,val));
      } else {
        std::size_t pos_equal = arg.find('=', 0);
        if(pos_equal != std::string::npos) {
          std::string key = arg.substr(0, pos_equal);
          std::string val = arg.substr(pos_equal+1);
          cmdline_args.insert(std::pair<std::string, std::string>(key,val));
        } else {
          std::cerr << "discarded unknown command line argument '"
                    << arg << "'" << std::endl;
        }
        arg_index = arg_index + 1;
      }
    }
  }
}

void imfind::print_cmdline_args(std::map<std::string, std::string> const &cmdline_args) {
  std::map<std::string, std::string>::const_iterator itr;
  std::cout << "Showing command line arguments:" << std::endl;
  for(itr=cmdline_args.begin(); itr!=cmdline_args.end(); ++itr) {
    std::cout << itr->first << " = " << itr->second << std::endl;
  }
}

//
// filesystem
//
bool imfind::fs_is_dir(std::string p) {
  struct stat p_stat;
  stat(p.c_str(), &p_stat);
  if( S_ISDIR(p_stat.st_mode) ) {
    return true;
  } else {
    return false;
  }
}

bool imfind::fs_dir_exists(std::string p) {
  struct stat p_stat;
  if(stat(p.c_str(), &p_stat) != 0) {
    return false;
  }
  if( S_ISDIR(p_stat.st_mode) ) {
    return true;
  } else {
    return false;
  }
}

bool imfind::fs_file_exists(std::string p) {
  struct stat p_stat;
  return (stat(p.c_str(), &p_stat) == 0);
}

bool imfind::fs_mkdir(std::string p) {
  int result = mkdir(p.c_str(), 0755);
  if(result==0) {
    return true;
  } else {
    return false;
  }
}

bool imfind::fs_mkdir_if_not_exists(std::string p) {
  if(!imfind::fs_dir_exists(p)) {
    return imfind::fs_mkdir(p);
  } else {
    return true;
  }
}

void imfind::fs_list_img_files(std::string dirpath,
                               std::vector<std::string> &imfn_list,
                               std::string filename_prefix) {
  std::string imfn_regex(".*(.jpg|.jpeg|.png|.bmp|.pnm)$");
  std::regex filename_regex(imfn_regex,
                            std::regex_constants::extended |
                            std::regex_constants::icase);

  struct dirent *dp;
  DIR *dfd = opendir(dirpath.c_str());
  if(!dfd) {
    std::cout << "fs_list_img_files(): cannot open dir: "
              << dirpath << std::endl;
    return;
  }

  while( (dp = readdir(dfd)) != NULL ) {
    std::string name(dp->d_name);
    if(name=="." || name=="..") {
      continue;
    }
    std::string path = dirpath + name;
    if(fs_is_dir(path)) {
      std::string subdir_name = filename_prefix + name + "/";
      std::string subdirpath = path + "/";
      std::vector<std::string> imfn_sublist;
      fs_list_img_files(subdirpath, imfn_sublist, subdir_name);
      imfn_list.insert(imfn_list.end(),
                       std::make_move_iterator(imfn_sublist.begin()),
                       std::make_move_iterator(imfn_sublist.end()));
    } else {
      if( std::regex_match(name, filename_regex) ) {
        if(filename_prefix.empty()) {
          imfn_list.push_back(name);
        } else {
          std::string name_with_prefix = filename_prefix + name;
          imfn_list.push_back(name_with_prefix);
        }
      } else {
        std::cout << "discarded: " << name << std::endl;
      }
    }
  }
  closedir(dfd);
}

//
// cache
//
bool imfind::cache_create(const std::string target_dir) {
  std::string cache_home = cache_homedir();
  std::vector<std::string> target_dirname_list;
  split(target_dir, '/', target_dirname_list);

  std::string cache_dir(cache_home);
  for(uint32_t i=1; i<target_dirname_list.size(); ++i) {
    cache_dir += target_dirname_list[i] + "/";
    if(!fs_dir_exists(cache_dir)) {
      if(!fs_mkdir(cache_dir)) {
        return false;
      }
    }
  }
  return true;
}

std::string imfind::cache_target2dir(const std::string target_dir) {
  std::string cache_home = cache_homedir();
  std::string target_dir_norm = target_dir;
  if(target_dir.front() == '/') {
    target_dir_norm = target_dir.substr(1); // discard the initial '/'
  }
  if(target_dir.back() != '/') {
    target_dir_norm = target_dir_norm + "/";
  }
  return cache_home + target_dir_norm;
}

bool imfind::cache_remove(const std::string target_dir) {
  std::string cache_dir = cache_target2dir(target_dir);
  std::cout << "cache_remove(): " << cache_dir << std::endl;

  struct dirent *dp;
  DIR *dfd = opendir(cache_dir.c_str());
  if(!dfd) {
    std::cout << "cannot open cache dir: "
              << cache_dir << std::endl;
    return false;
  }

  while( (dp = readdir(dfd)) != NULL ) {
    std::string name(dp->d_name);
    if(name=="." || name=="..") {
      continue;
    }
    std::string filepath = cache_dir + name;
    if(fs_is_dir(filepath)) {
      continue; // avoid deleting directories
    } else {
      int file_result = unlink(filepath.c_str());
      std::cout << "file=" << filepath << ", "
                << "file_result=" << file_result
                << std::endl;
      if(file_result!=0) {
        return false;
      }
    }
  }
  int dir_result = rmdir(cache_dir.c_str());
  // rmdir() failure does not indicate an error
  // because other non-empty subfolders may exits.
  return true;
}

bool imfind::cache_clear_all() {
  std::string cache_home = cache_homedir();
  std::cout << "Clearing cache: " << cache_home << std::endl;

  struct dirent *dp;
  DIR *dfd = opendir(cache_home.c_str());
  if(!dfd) {
    std::cout << "cannot open cache home: "
              << cache_home << std::endl;
    return false;
  }

  while( (dp = readdir(dfd)) != NULL ) {
    std::string name(dp->d_name);
    if(name=="." || name=="..") {
      continue;
    }
    std::string filepath = cache_home + name;
    if(fs_is_dir(filepath)) {
      std::string dirpath = filepath + "/";
      if(!cache_clear_all_subfolders(dirpath)) {
        return false;
      }
    } else {
      int file_result = unlink(filepath.c_str());
      if(file_result!=0) {
        return false;
      }
    }
  }

  return true;
}

bool imfind::cache_clear_all_subfolders(std::string dirpath) {
  // for safety, ensure that dirpath is a subfolder of cache_home
  std::string cache_home = cache_homedir();
  std::string prefix = dirpath.substr(0, cache_home.length());
  if(prefix!=cache_home) {
    std::cout << "path must be a subfolder in cache" << std::endl;
    return false;
  }

  struct dirent *dp;
  DIR *dfd = opendir(dirpath.c_str());
  if(!dfd) {
    std::cout << "cannot open "
              << dirpath << std::endl;
    return false;
  }

  while( (dp = readdir(dfd)) != NULL ) {
    std::string name(dp->d_name);
    if(name=="." || name=="..") {
      continue;
    }
    std::string filepath = dirpath + name;
    if(fs_is_dir(filepath)) {
      std::string dirpath = filepath + "/";
      if(!cache_clear_all_subfolders(dirpath)) {
        return false;
      }
    } else {
      int file_result = unlink(filepath.c_str());
      if(file_result!=0) {
        return false;
      }
    }
  }
  int dir_result = rmdir(dirpath.c_str());
  if(dir_result!=0) {
    return false;
  }

  return true;
}

std::string imfind::imfind_homedir() {
  std::string user_home = getenv("HOME");
  return user_home + "/.imfind/";
}

std::string imfind::cache_homedir() {
  std::string imfind_home = imfind_homedir();
  return imfind_home + "cache/";
}

std::string imfind::test_homedir() {
  std::string imfind_home = imfind_homedir();
  return imfind_home + "test/";
}

std::string imfind::asset_homedir() {
  std::string imfind_home = imfind_homedir();
  return imfind_home + "asset/";
}

bool imfind::init_all_homedir() {
  if( !fs_mkdir_if_not_exists(imfind_homedir()) ) {
    std::cout << "failed to create imfind home folder: "
              << imfind_homedir() << std::endl;
    return false;
  }
  if( !fs_mkdir_if_not_exists(test_homedir()) ) {
    std::cout << "failed to create imfind test folder: "
              << test_homedir() << std::endl;
    return false;
  }
  if( !fs_mkdir_if_not_exists(cache_homedir()) ) {
    std::cout << "failed to create imfind cache folder: "
              << cache_homedir() << std::endl;
    return false;
  }
  if( !fs_mkdir_if_not_exists(asset_homedir()) ) {
    std::cout << "failed to create imfind asset folder: "
              << asset_homedir() << std::endl;
    return false;
  }

  return true;
}

void imfind::split(const std::string &s,
                   const char separator,
                   std::vector<std::string> &chunks) {
  chunks.clear();
  std::vector<std::size_t> seperator_index_list;

  std::size_t start = 0;
  std::size_t sep_index;
  while ( start < s.length() ) {
    sep_index = s.find(separator, start);
    if ( sep_index == std::string::npos ) {
      break;
    } else {
      chunks.push_back( s.substr(start, sep_index - start) );
      start = sep_index + 1;
    }
  }
  if ( start != s.length() ) {
    chunks.push_back( s.substr(start) );
  }
}

//
// testdir
//
std::string imfind::testdir_create(const std::string test_name) {
  std::string test_home = test_homedir();
  std::string testdir = test_home + "/" + test_name + "/";
  if(!fs_dir_exists(testdir)) {
    if(!fs_mkdir(testdir)) {
      return "";
    }
  }
  return testdir;
}

bool imfind::testdir_remove(const std::string test_name) {
  std::string test_home = test_homedir();
  std::string testdir = test_home + "/" + test_name + "/";

  struct dirent *dp;
  DIR *dfd = opendir(testdir.c_str());
  if(!dfd) {
    std::cout << "cannot open testdir: "
              << testdir << std::endl;
    return false;
  }

  while( (dp = readdir(dfd)) != NULL ) {
    std::string name(dp->d_name);
    if(name=="." || name=="..") {
      continue;
    }
    std::string filepath = testdir + name;
    int file_result = unlink(filepath.c_str());
    if(file_result!=0) {
      return false;
    }
  }
  int dir_result = rmdir(testdir.c_str());
  if(dir_result!=0) {
    return false;
  } else {
    return true;
  }
}

//
// Filename list
//
void imfind::load_fn_list(const std::string fn,
                          std::vector<std::string> &fnlist) {
  fnlist.clear();
  std::ifstream f(fn);
  if(f.is_open()) {
    std::string filename;
    while(std::getline(f, filename)) {
      fnlist.push_back(filename);
    }
    f.close();
  } else {
    std::cout << "imfind::load_fn_list(): failed to open file "
              << fn << std::endl;
  }
}

//
// timestamp
//
std::string imfind::now_timestamp() {
  // see https://en.cppreference.com/w/cpp/chrono/c/time
  std::time_t result = std::time(nullptr);
  return std::string(std::asctime(std::localtime(&result)));
}

bool imfind::debug_is_fn_prefix_same(std::string f1, std::string f2) {
  std::string n1str, n2str;
  std::size_t dash1 = f1.find('-', 0);
  if(dash1 == std::string::npos) {
    n1str = f1;
  } else {
    n1str = f1.substr(0, dash1);
  }

  std::size_t dash2 = f2.find('-', 0);
  if(dash2 == std::string::npos) {
    n2str = f2;
  } else {
    n2str = f2.substr(0, dash2);
  }

  uint32_t n1 = std::stoi(n1str);
  uint32_t n2 = std::stoi(n2str);
  if(n1==n2) {
    return true;
  } else {
    return false;
  }
}

uint32_t imfind::debug_count_fn_copy(std::string fn, std::vector<std::string> fnlist) {
  uint32_t copy_count = 0;
  for (uint32_t i=0; i<fnlist.size(); ++i) {
    if(debug_is_fn_prefix_same(fn, fnlist.at(i))) {
      copy_count += 1;
    }
  }
  return copy_count;
}

void imfind::read_imfeat(const std::string feat_fn,
                         uint8_t data_type_id,
                         uint32_t feat_dim,
                         std::vector<uint8_t> &imfeat) {
  std::cout << "read_imfeat()" << std::endl;
  // ASSUMPTION: binary data is saved in the following format
  // feature_dimension (uint32_t, 4 byte)
  // data_type_id (uint8_t, 1 byte)
  // data follows ...
  FILE *feat_f = fopen(feat_fn.c_str(), "rb");
  if ( feat_f == NULL ) {
    std::cerr << "Failed to open tflitefeat file for reading: "
              << feat_fn << std::endl;
    return;
  }
  std::size_t read_count;
  uint32_t feat_dim_got;
  read_count = fread( &feat_dim_got, sizeof(uint32_t), 1, feat_f);
  if(read_count != 1) {
    std::cerr << "Error: reading feat_dim " << std::endl;
    fclose(feat_f);
    return;
  }
  if(feat_dim_got != feat_dim) {
    std::cerr << "Error: feature dimension mismatch, "
              << feat_dim_got << " != " << feat_dim
              << std::endl;
    fclose(feat_f);
    return;
  }

  uint8_t data_type_id_got;
  read_count = fread( &data_type_id_got, sizeof(uint8_t), 1, feat_f);
  if(read_count != 1) {
    std::cerr << "Error: reading data_type_id " << std::endl;
    fclose(feat_f);
    return;
  }
  if(data_type_id_got!=data_type_id) {
    std::cerr << "Error: data_type_id mismatch, "
              << data_type_id_got << " != " << data_type_id
              << std::endl;
    fclose(feat_f);
    return;
  }

  // find out the number of stored vectors based on file size
  long pos0, pos1;
  pos0 = ftell(feat_f);
  fseek(feat_f, 0, SEEK_END);
  pos1 = ftell(feat_f);
  long tflitefeat_size = pos1 - pos0;
  uint32_t tflitefeat_count = (pos1 - pos0) / sizeof(uint8_t);

  fseek(feat_f, pos0, SEEK_SET);
  imfeat.resize(tflitefeat_count);
  read_count = fread( imfeat.data(),
                      sizeof(uint8_t),
                      tflitefeat_count,
                      feat_f);
  if(read_count != tflitefeat_count) {
    std::cerr << "Read only " << read_count << "/" << tflitefeat_count
              << " entries" << std::endl;
    fclose(feat_f);
    return;
  }
  fclose(feat_f);
}

void imfind::print_imhist(const std::vector<uint8_t> &rgb,
                          const std::string name) {
  std::map<unsigned int, uint32_t> pixel_hist;
  for(std::size_t i=0; i<rgb.size(); ++i) {
    if(pixel_hist.count(rgb.at(i)) == 0) {
      pixel_hist[ rgb.at(i) ] = 1;
    } else {
      pixel_hist[ rgb.at(i) ] = pixel_hist[ rgb.at(i) ] + 1;
    }
  }
  std::cout << name << ": [";
  std::map<unsigned int, uint32_t>::const_iterator itr = pixel_hist.begin();
  std::cout << (int) itr->first << ":" << itr->second;
  ++itr;
  for(; itr!=pixel_hist.end(); ++itr) {
    std::cout << ", " << (int) itr->first << ":" << itr->second;
  }
  std::cout << "]" << std::endl;
}
