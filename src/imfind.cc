/*
** imfind : a command line tool for visual search of images
**
** Author: Abhishek Dutta <adutta _at_ robots.ox.ac.uk>
** Date: Dec. 13, 2019
**
*/
#include "license_text.h" // license displayed using --license command
#include "help_text.h"    // user guide displayed using --help command
#include "export_html.h"  // templates for exporting results as HTML
#include "util.h"         // basic utilities
#include "log.h"          // a minimal logger for imfind

// data processing blocks
#include "blk_flist.h"    // generates a list of image filenames in a folder
#include "blk_imread.h"   // loads an image into memory (resizes if required)
#include "blk_null_sink.h"// pulls out entries from out queue (i.e. result) and sinks them
#include "blk_imwrite.h"  // writes image form memory to filesystem
#include "blk_imfeat.h"   // extract features from an image loaded into memory
#include "blk_feat_index.h" // creates an index of image features for quick search (e.g. nearest neighbour)
#include "blk_report.h"   // generates a report (e.g html or csv) to summarize matching results

#include <iostream>
#include <map>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <thread>
#include <algorithm>
#include <fstream>
#include <ctime>
#include <iomanip>

#define IMFIND_VERSION_MAJOR 1
#define IMFIND_VERSION_MINOR 0
#define IMFIND_VERSION_PATCH 1

bool IMFIND_DEBUG = false;

int main(int argc, char **argv) {
  if(argc < 2) {
    std::cout << "Usage: " << std::endl
              << argv[0] << " COMMAND [COMMAND-FLAGS] [COMMAND-PARAM]"
              << " [OUTPUT-FLAGS] [OUTPUT-PARAM]"
              << " [TARGET_DIRECTORY]"
              << std::endl
              << argv[0] << " --help for more details."
              << std::endl;

    return EXIT_FAILURE;
  }

  std::string cmd(argv[1]);

  if(cmd == "--version") {
    std::cout << "imfind "
              << IMFIND_VERSION_MAJOR << "."
              << IMFIND_VERSION_MINOR << "."
              << IMFIND_VERSION_PATCH << std::endl
              << "Copyright 2019-2020 Abhishek Dutta, Visual Geometry Group, University of Oxford." << std::endl
              << "License BSD-3-Clause <https://opensource.org/licenses/BSD-3-Clause>" << std::endl
              << "Use --license flag to show the license of imfind and its dependencies." << std::endl
              << std::endl
              << "Written by Abhishek Dutta." << std::endl
              << "Developed and maintained by Visual Geometry Group <https://www.robots.ox.ac.uk/~vgg/>"
              << std::endl;
    return EXIT_SUCCESS;
  }

  if(cmd == "--license") {
    std::cout << IMFIND_LICENSE_STR << std::endl;
    return EXIT_SUCCESS;
  }

  if(cmd == "--help") {
    std::cout << IMFIND_HELP_STR << std::endl;
    return EXIT_SUCCESS;
  }

  if(cmd == "--clear-cache") {
    imfind::cache_clear_all();
    return EXIT_SUCCESS;
  }

  if(argc==2) {
    std::cout << "TARGET_DIRECTORY not specified" << std::endl;
    return EXIT_FAILURE;
  }

  // parse all command line arguments
  std::map<std::string, std::string> cmdline_args;
  imfind::parse_cmdline_args(argc, (const char **)argv, cmdline_args);

  bool force_recompute = false;
  if(cmdline_args.count("--force") == 1) {
    force_recompute = true;
  }

  std::string target_dir = argv[argc-1];
  if(target_dir.back() != '/') {
    target_dir += '/';
  }
  if(!imfind::fs_dir_exists(target_dir)) {
    std::cout << "TARGET_DIRECTORY="
              << target_dir << "does not exist" << std::endl;
    return EXIT_FAILURE;
  }

  if(!imfind::init_all_homedir()) {
    std::cout << "failed to initialize home directories"
              << std::endl;
    return EXIT_FAILURE;
  }

  if(!imfind::cache_create(target_dir)) {
    std::cout << "failed to create cache for target: "
              << target_dir << std::endl;
    return EXIT_FAILURE;
  }

  std::string cache_dir = imfind::cache_target2dir(target_dir);
  std::string asset_dir = imfind::asset_homedir();

  if(cmdline_args.count("--debug") == 1) {
    IMFIND_DEBUG = true;
    std::clog.clear();
  } else {
    std::clog.setstate(std::ios_base::failbit);
  }
  std::clog << "cache_dir=" << cache_dir << std::endl;
  std::clog << "asset_dir=" << asset_dir << std::endl;

  if(cmd == "--is-identical") {
    //bool blk::debug = true;

    // declare all processing blocks
    blk_flist flist;
    blk_imread imread;
    blk_imfeat imfeat;
    blk_feat_index feat_index;
    blk_report report;

    // configure all processing blocks
    std::string tflite_model_fn = "model/efficientnet/efficientnet_lite0_fp32.tflite";
    int max_threads = std::thread::hardware_concurrency() - 2;
    flist.output_buffer_size(std::numeric_limits<uint32_t>::max());
    flist.src_path(target_dir);
    std::size_t file_count = flist.file_count();
    imread.output_buffer_size(999);
    // efficientnet_lite0_fp32.tflite: The expected image is 224 x 224, with
    // three channels (red, blue, and green) per pixel. Each element in the
    // tensor is a value between min and max, where (per-channel) min is
    // [-0.9921875] and max is [1.0].
    imread.d_conf_uint32["wanted_width"] = 224;
    imread.d_conf_uint32["wanted_height"] = 224;
    imread.d_conf_uint32["wanted_nchannel"] = 3;
    imread.d_conf_string["wanted_dtype"] = "float";
    imread.d_conf_bool["element_value_scale"] = true;
    imread.d_conf_float["element_value_min"] = -0.9921875F;
    imread.d_conf_float["element_value_max"] = 1.0F;
    imread.d_conf_string["resize_using"] = "stb_imresize"; // or, tflite_imresize
    imfeat.output_buffer_size(999);
    imfeat.d_conf_string["tflite_model_fn"] = asset_dir + tflite_model_fn;
    imfeat.d_conf_uint32["tflite_model_input_index"] = 0;
    imfeat.d_conf_uint32["tflite_model_output_index"] = 155; // 7x7x192
    feat_index.d_conf_uint32["feature_dimension"] = 192;
    imfeat.d_conf_string["tflite_model_output_apply"] = "AveragePool2D";
    imfeat.d_conf_uint32["tflite_interpreter_num_threads"] = std::max(1, max_threads);
    imfeat.get_ready_for_inference();
    feat_index.d_conf_uint32["estimated_file_count"] = file_count;
    feat_index.d_conf_float["index_search_radius"] = 1e-8;
    feat_index.d_conf_string["cache_dir"] = cache_dir;
    feat_index.get_ready_for_indexing();
    report.d_conf_string["target_dir"] = target_dir;
    report.d_conf_string["cache_dir"] = cache_dir;
    report.d_conf_string["html_report_fn"] = cache_dir + "is-identical.html";
    report.d_conf_string["csv_report_fn"] = cache_dir + "is-identical.csv";
    report.d_conf_string["cmd"] = cmd;
    report.d_conf_uint32["estimated_file_count"] = file_count;
    report.d_conf_uint32["image_height"] = 200;
    std::cout << "For results, see file://" << report.d_conf_string.at("html_report_fn")
              << std::endl;

    // connect all the processing blocks
    // Note: flist is the source block which produces a list of filenames
    imread.in(&flist);            // get a filename and read RGB data
    imfeat.in(&imread);           // extract features
    feat_index.in(&imfeat);       // index features and use nearest neighbour
    report.in(&feat_index, true); // wait until finish

    return EXIT_SUCCESS;
  } // end of is-identical


  std::cout << cmd << " has not been implemented yet!"
            << std::endl;

  return EXIT_SUCCESS;
}
