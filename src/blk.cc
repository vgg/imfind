#include "blk.h"

blk::blk(std::string blk_name) {
  d_in = nullptr;
  d_blk_name = blk_name;
  d_output_buffer_size = 2;
  d_has_task = true;
  d_debug = false;
}

blk::~blk() {
  //std::cout << "destructing " << d_blk_name << std::endl;
}

void blk::output_buffer_size(uint32_t buffer_size) {
  d_output_buffer_size = buffer_size;
}

uint32_t blk::output_buffer_size() {
  return d_output_buffer_size;
}

bool blk::has_task() const {
  return (d_has_task || !d_task_queue.empty());
}

std::string blk::blk_name() const {
  return d_blk_name;
}

void blk::print_flow_graph() const {
  std::stack<std::string> blk_name_list;
  blk_name_list.push(blk_name());
  blk *in = d_in;
  while(in != nullptr) {
    blk_name_list.push(in->blk_name());
    in = in->d_in;
  }

  while(!blk_name_list.empty()) {
    std::cout << "[" << blk_name_list.top() << "]";
    blk_name_list.pop();
    if(!blk_name_list.empty()) {
      std::cout << " -> ";
    }
  }
  std::cout << std::endl;
}

void blk::in(blk* in, bool wait_until_finish) {
  d_in = in;
  d_processed_payload_count = std::async(std::launch::async, &blk::on_in, this);

  if(wait_until_finish) {
    d_processed_payload_count.wait();
  }
}

blk* blk::in() {
  return d_in;
}

uint32_t blk::on_in() {
  uint32_t processed_payload_count = 0;
  d_has_task = true;
  d_start_time = std::chrono::system_clock::now();
  while(d_in->has_task()) {
    blk_payload in_payload;
    bool got_task = d_in->get_one_task(in_payload, d_blk_name);

    if(got_task) {
      process_one_task(in_payload);
      processed_payload_count = processed_payload_count + 1;
    }
  }
  d_end_time = std::chrono::system_clock::now();
  auto elapsed = d_end_time - d_start_time;
  if(d_debug) {
    std::cout << blk_name() << " : completed in " << elapsed.count() << " sec.";
  }
  d_has_task = false;
  d_get_task_condition.notify_all();
  on_all_done();
  return processed_payload_count;
}

bool blk::get_one_task(blk_payload &payload, const std::string caller_blk_name) {
  std::unique_lock<std::mutex> lock(d_get_one_task_mutex);
  while(d_task_queue.empty() && d_has_task) {
    d_get_task_condition.wait(lock);
  }

  // the above while() loop may get over when d_has_task becomes false
  if(!d_task_queue.empty()) {
    payload = std::move(*d_task_queue.front());
    d_task_queue.pop();
    if(d_debug) {
      std::cout << blk_name() << "::get_one_task(" << caller_blk_name
                << ") " << payload.d_string << std::endl;
    }
    d_add_task_condition.notify_one();
    return true;
  } else {
    return false;
  }
}

void blk::add_one_task(blk_payload payload) {
  std::shared_ptr<blk_payload> task(std::make_shared<blk_payload>(std::move(payload)));
  std::unique_lock<std::mutex> lock(d_add_one_task_mutex);

  if(d_output_buffer_size) {
    while( (d_task_queue.size() >= d_output_buffer_size) ) {
      d_add_task_condition.wait(lock);
    }
  }
  if(d_debug) {
    std::cout << blk_name() << "::add_one_task() "
              << task.get()->d_string << std::endl;
  }
  d_task_queue.push(task);
  d_get_task_condition.notify_one();
}

void blk::process_one_task(const blk_payload in_payload) {
  return; // no processing done by default, needs to be overridden by blk implementation
}


void blk::skip_one_task(const blk_payload &in_payload, std::string reason) const {
  return; // nothing done by default
}

void blk::on_all_done() {
  return; // nothing done by default
}

void blk::print_task_queue() const {

}
