#ifndef BLK_IMFEAT_H
#define BLK_IMFEAT_H

// extract features from an image
//
// Author: Abhishek Dutta <adutta@robots.ox.ac.uk>
// Date: 7 April 2020

#include "blk_payload.h"
#include "blk.h"

#include <iostream>
#include <cmath>
#include <iomanip>
#include <utility>

#include "tensorflow/lite/builtin_op_data.h"
#include "tensorflow/lite/interpreter.h"
#include "tensorflow/lite/kernels/register.h"
#include "tensorflow/lite/string_util.h"
#include "tensorflow/lite/optional_debug_tools.h"

class blk_imfeat : public blk {
 public:
  blk_imfeat();
  void get_ready_for_inference();
 private:
  void process_one_task(const blk_payload in_payload) override;

  void load_model();

  std::unique_ptr<tflite::FlatBufferModel> d_tflite_model;
  std::unique_ptr<tflite::Interpreter> d_interpreter;
};

#endif
