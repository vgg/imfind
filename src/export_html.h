#ifndef IMFIND_EXPORT_HTML_TEXT
#define IMFIND_EXPORT_HTML_TEXT

const char *IMFIND_HTML_EXPORT_HEAD_STR = R"TEXT(<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>imfind</title>
    <meta name="author" content="imfind">
    <meta name="description" content="imfind result exported as HTML using --export-html flag">
    <style>
body { font-family:sans; background-color:white; margin:1ch; font-size:12pt; }
input[type="text"] { width:2em; }
img { margin:1em; }
.item { border:1px solid #cccccc; margin:2em 1em; padding:0.4em; }
.item div { display:inline; }
    </style>
  </head>
)TEXT";


const char *IMFIND_HTML_EXPORT_TAIL_STR = R"TEXT(</html>)TEXT";

const char *IMFIND_HTML_EXPORT_SAME_JS_STR = R"TEXT(<script>
function imfind_init_html_ui() {
  init_toolbar();
}

function init_toolbar() {
  var toolbar = document.getElementById('toolbar');
  toolbar.innerHTML = '';

  // selector for number of images in a set
  var same_size_list = Object.keys(same_stat);
  this.same_size_dropdown = document.createElement('select');
  for(var i=0; i<same_size_list.length; ++i) {
    var oi = document.createElement('option');
    oi.innerHTML = 'Same set containing ' + same_size_list[i] + ' images';
    oi.setAttribute('value', same_size_list[i]);
    this.same_size_dropdown.appendChild(oi);
  }
  this.same_size_dropdown.addEventListener('change', same_size_dropdown_onchage.bind(this));

  var label_same_size = document.createElement('span');
  label_same_size.innerHTML = 'Select a subset &nbsp;';
  toolbar.appendChild(label_same_size);
  toolbar.appendChild(same_size_dropdown);

  this.pageno_dropdown = document.createElement('select');
  pageno_dropdown.setAttribute('title', 'Jump to page number');
  var label_pageno = document.createElement('span');
  label_pageno.innerHTML = '&nbsp;Jump to&nbsp;';
  toolbar.appendChild(label_pageno);
  toolbar.appendChild(this.pageno_dropdown);

  this.set_perpage_input = document.createElement('input');
  this.set_perpage_input.setAttribute('type', 'text');
  this.set_perpage_input.setAttribute('value', '10');
  this.set_perpage_input.setAttribute('title', 'Number of entries per page');
  this.set_perpage_input.addEventListener('change', function(e) {
    this.reload_current_page();
  }.bind(this));
  var label_perpage = document.createElement('span');
  label_perpage.innerHTML = 'Entries per page&nbsp;';
  label_perpage.setAttribute('style', 'margin-left:1em;');
  toolbar.appendChild(label_perpage);
  toolbar.appendChild(this.set_perpage_input);

  this.imsize_input = document.createElement('input');
  this.imsize_input.setAttribute('type', 'text');
  this.imsize_input.setAttribute('value', '200');
  this.imsize_input.setAttribute('title', 'Image Width (in pixels)');

  this.imsize_input.addEventListener('change', function(e) {
    this.reload_current_page();
  }.bind(this))
  var label_imsize = document.createElement('span');
  label_imsize.setAttribute('style', 'margin-left:1em;');
  label_imsize.innerHTML = 'Image width (in pixels)&nbsp;';
  var label_percent = document.createElement('span');

  toolbar.appendChild(label_imsize);
  toolbar.appendChild(this.imsize_input);

  if(same_size_list.length) {
    this.same_size_dropdown.selectedIndex = 0;
    set_same_size(same_size_list[0]);
  } else {
    same_size_dropdown.selectedIndex = -1;
  }
}

function same_size_dropdown_onchage(e) {
  var same_size = e.target.options[e.target.selectedIndex].value;
  set_same_size(same_size);
}

function set_same_size(same_size) {
  var entries_per_page = parseInt(this.set_perpage_input.value);
  var page_count = Math.ceil(same_stat[same_size].length / entries_per_page);
  this.pageno_dropdown.innerHTML = '';
  for(var pageno=0; pageno < page_count; ++pageno) {
    var oi = document.createElement('option');
    oi.innerHTML = 'Page ' + (pageno + 1);
    oi.setAttribute('value', pageno);
    oi.setAttribute('data-samesize', same_size);
    this.pageno_dropdown.appendChild(oi);
  }
  this.pageno_dropdown.addEventListener('change', function(e) {
    var oi = e.target.options[e.target.selectedIndex];
    var same_size = oi.dataset.samesize;
    var pageno = oi.value;
    show_page(same_size, pageno);
  }.bind(this));
  if(page_count > 0) {
    this.pageno_dropdown.selectedIndex = 0;
    show_page(same_size, 0); // show first page by default
  } else {
    this.pageno_dropdown.selectedIndex = -1;
  }
}

function reload_current_page() {
  this.show_page(this.current_same_size, this.current_pageno);
}

function show_page(same_size, pageno) {
  this.current_same_size = same_size;
  this.current_pageno = pageno;
  this.current_imsize = parseFloat(this.imsize_input.value);

  var entries_per_page = parseInt(this.set_perpage_input.value);
  var start = entries_per_page * pageno;
  var end = Math.min(start + entries_per_page, same_stat[same_size].length);
  var content = document.getElementById('content');
  content.innerHTML = '';
  for( var i=start; i < end; ++i ) {
    var same_index = same_stat[same_size][i];
    var same = same_list[same_index];
    var samediv = document.createElement('div');
    samediv.setAttribute('class', 'item');
    var samedivtitle = document.createElement('div');
    samedivtitle.innerHTML = '[' + (i+1) + ']';
    samediv.appendChild(samedivtitle);
    for ( var j=0; j < same.length; ++j) {
      var img = document.createElement('img');
      img.setAttribute('src', target_dir + flist[ same[j] ]);
      img.setAttribute('width', this.current_imsize);
      img.setAttribute('title', flist[ same[j] ]);
      samediv.appendChild(img);
    }
    content.appendChild(samediv);
  }
}

</script>)TEXT";

#endif
