#include "blk_flist.h"

blk_flist::blk_flist() : blk("blk_flist") {
}

void blk_flist::process_one_task(const blk_payload in_payload) {
  blk_payload out_payload;
  out_payload.d_string = in_payload.d_string;
  add_one_task(out_payload);
}

void blk_flist::src_path(std::string src_path) {
  d_conf_string["src_path"] = src_path;
  d_file_add_count = std::async(std::launch::async,
                                &blk_flist::ls_and_queue,
                                this,
                                d_conf_string["src_path"], "");
}

uint32_t blk_flist::file_count() {
  return d_file_add_count.get();
}

uint32_t blk_flist::ls_and_queue(std::string dirpath,
                                 std::string filename_prefix) {
  uint32_t file_add_count = 0;
  blk::d_has_task = true;

  std::string imfn_regex(".*(.jpg|.jpeg|.png|.bmp|.pnm)$");
  std::regex filename_regex(imfn_regex,
                            std::regex_constants::extended |
                            std::regex_constants::icase);

  struct dirent *dp;
  DIR *dfd = opendir(dirpath.c_str());
  if(!dfd) {
    std::cout << "ls_and_queue(): cannot open dir: "
              << dirpath << std::endl;
    return 0;
  }

  while( (dp = readdir(dfd)) != NULL ) {
    std::string name(dp->d_name);
    if(name=="." || name=="..") {
      continue;
    }
    std::string path = dirpath + name;
    if(fs_is_dir(path)) {
      std::string subdir_name = filename_prefix + name + "/";
      std::string subdirpath = path + "/";
      uint32_t subdir_file_add_count = ls_and_queue(subdirpath, subdir_name);
      file_add_count = file_add_count + subdir_file_add_count;
    } else {
      if( std::regex_match(name, filename_regex) ) {
        blk_payload payload;
        payload.m_string["src_path"] = d_conf_string["src_path"];
        if(filename_prefix.empty()) {
          payload.d_string = name;
        } else {
          payload.d_string = filename_prefix + name;
        }
        add_one_task(payload);
        file_add_count = file_add_count + 1;
      } else {
        std::cout << "DISCARDED " << name << ": file is not an image." << std::endl;
      }
    }
  }
  closedir(dfd);
  blk::d_has_task = false;
  return file_add_count;
}

bool blk_flist::fs_is_dir(std::string p) {
  struct stat p_stat;
  stat(p.c_str(), &p_stat);
  if( S_ISDIR(p_stat.st_mode) ) {
    return true;
  } else {
    return false;
  }
}
