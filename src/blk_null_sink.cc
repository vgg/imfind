#include "blk_null_sink.h"

blk_null_sink::blk_null_sink() : blk("blk_null_sink") {
}

void blk_null_sink::process_one_task(const blk_payload in_payload) {
  //std::cout << "blk_null_sink::process_one_task() : " << in_payload.d_string << std::endl;
  return; // no processing done by default, needs to be overridden by blk implementation
}
