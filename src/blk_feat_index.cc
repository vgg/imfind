#include "blk_feat_index.h"

blk_feat_index::blk_feat_index() : blk("blk_feat_index") {
}

void blk_feat_index::get_ready_for_indexing() {
  d_index_fn = "faiss_IndexFlatL2.bin";
  d_index = faiss::IndexFlatL2(d_conf_uint32.at("feature_dimension"));

  d_filename.reserve(d_conf_uint32.at("estimated_file_count"));
  d_feature.reserve(d_conf_uint32.at("estimated_file_count") * d_conf_uint32.at("feature_dimension"));
}

void blk_feat_index::process_one_task(const blk_payload in_payload) {
  blk_payload out_payload;
  out_payload.d_string = in_payload.d_string;

  float search_radius = d_conf_float.at("index_search_radius");
  faiss::RangeSearchResult results(d_filename.size());
  d_index.range_search(1,
                       in_payload.v_float.data(),
                       search_radius,
                       &results);
  if(results.lims[0] != results.lims[1]) {
    //std::cout << in_payload.d_string << " : ";
    for(std::size_t qj=results.lims[0]; qj < results.lims[1]; ++qj) {
      //std::cout << d_filename[ results.labels[qj] ] << "(" << std::fixed << std::setprecision(20) << results.distances[qj] << "), ";
      out_payload.v_string.push_back(d_filename[ results.labels[qj] ]);
      out_payload.v_float.push_back(results.distances[qj]);
      out_payload.v_uint32.push_back(results.labels[qj]);
    }
  }

  d_index.add(1, in_payload.v_float.data());
  d_filename.push_back(in_payload.d_string);

  for(uint32_t i=0; i<in_payload.v_float.size(); ++i) {
    d_feature.push_back(in_payload.v_float.at(i));
  }

  add_one_task(out_payload);
}

void blk_feat_index::on_all_done() {
  // @todo-future
  //faiss::write_index(&d_index, d_index_fn.c_str());
}
