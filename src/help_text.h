#ifndef IMFIND_HELP_TEXT
#define IMFIND_HELP_TEXT

const char *IMFIND_HELP_STR = R"TEXT(## NAME
imfind - a command line tool for visual search of images.

## SYNOPSIS
imfind COMMAND [COMMAND-FLAGS] [COMMAND-OPTIONS] [OUTPUT-FLAGS] [OUTPUT-OPTIONS] TARGET_DIR

## DESCRIPTION
imfind is a command line tool for visual search of images. It processes a
directory containing images and finds out all the images matching visual
content defined by the user.

## OPTIONS
imfind supports the following commands:

--is-identical : find duplicates (i.e. is same pixelwise)
--has-face     : find images containing one or more human face

--version     : about imfind software
--license     : license text
--debug       : show additional details useful for debugging
--help        : show this help message

and the following command flags:
--count       : specify count of object (e.g. face)
--force       : force recomputation of everything
--export-html : export results as an HTML file

## EXAMPLES
imfind --is-identical --export-html /data/imagenet  # find exact copies
imfind --has-face -count 2 /data/facedb             # filter images with two human faces

## AUTHOR
Abhishek Dutta < adutta _at_ robots.ox.ac.uk >

## Website
https://www.robots.ox.ac.uk/~vgg/software/imfind
https://gitlab.com/vgg/imfind

## COPYRIGHT
Copyright 2019-2020 Abhishek Dutta, Visual Geometry Group, University of Oxford.
License BSD-3-Clause <https://opensource.org/licenses/BSD-3-Clause>
Use --license flag to show the license of imfind and its dependencies.)TEXT";

const char *IMFIND_HELP_FUTURE_STR = R"TEXT(## NAME
imfind - a command line tool for visual search of images.

## SYNOPSIS
imfind COMMAND [COMMAND-FLAGS] [COMMAND-OPTIONS] [OUTPUT-FLAGS] [OUTPUT-OPTIONS] TARGET_DIR

## DESCRIPTION
imfind is a command line tool for visual search of images. It processes a
directory containing images and finds out all the images matching visual
content defined by the user.

## OPTIONS
imfind supports the following commands:

--is-same     : find exact duplicates (i.e. is same pixelwise)
--is-copy     : find copies (e.g. scaled, reflected, rotated, etc.)
--is-topy     : find transformed copies (e.g. cropped at edges, etc)
--is-vsim     : find visually similar images

--has-txt     : light weight answer to "does it have text?"
--has-face    : light weight answer to "does it have a face?"
--has-obj     : light weight answer to "does it have an object?"

--find-face   : face detector and verification using ?
--find-obj    : object detector using ?
--find-txt    : text spotting using ?

--like-url    : instance search using VISE
--like-file   : instance search using VISE

--build-voc   : build visual vocabular based on images in a folder
--clear-cache : clear cache for a particular folder
--force       : force recomputation of everything

--export-html : export results as an HTML file

--help
--version     : about imfind software
--license     : license text

:)            : indicates positive feedback about imfind
:(            : indicates negative feedback about imfind

and the following command flags:

--region      : to describe image region in a query image
--like        : to provide a visual example of query
--force       : force recomputation of everything

## EXAMPLES
imfind --is-same /data/imagenet                   # exact copies
imfind --is-copy /data/imagenet                   # scale, rotate, ...
imfind --is-vsim /data/imagenet                   # visually similar

imfind --like-file query.jpg /data/ok5k           # instance search
imfind --like-url http://test.com/query.jpg /data # instance search
imfind --like-file qry.jpg --region 2,2,8,8 /data # instance search

imfind --has-txt --like 'london' /data/bbcnews    # contains text?
imfind --has-face /data/vggface                   # contains face?
imfind --has-obj --like car.jpg /data/mscoco      # contains a car?

imfind --find-face /data/vggface                  # detect human face
imfind --find-face --like obama.jpg /data/vggface # detect+verify face
imfind --find-txt /data/bbcnews                   # detect+recog. text
imfind --find-txt --like 'london' /data/bbcnews   # detect given text
imfind --find-obj --like dog.jpg /data/mscoco     # detect dog

## AUTHOR
Abhishek Dutta < adutta _at_ robots.ox.ac.uk >

## COPYRIGHT
Copyright 2019-2020 Abhishek Dutta, Visual Geometry Group, University of Oxford.
License BSD-3-Clause <https://opensource.org/licenses/BSD-3-Clause>
Use --license flag to show the license of imfind and its dependencies.
)TEXT";

#endif
