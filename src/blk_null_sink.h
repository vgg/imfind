#ifndef BLK_NULL_SINK_H
#define BLK_NULL_SINK_H

// removes all items from the input block
//
// Author: Abhishek Dutta <adutta@robots.ox.ac.uk>
// Date: 5 April 2020

#include "blk_payload.h"
#include "blk.h"

#include <iostream>

class blk_null_sink : public blk {
public:
  blk_null_sink();
private:
  void process_one_task(const blk_payload in_payload);
};

#endif
