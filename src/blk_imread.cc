#include "blk_imread.h"

#ifndef STB_IMAGE_RESIZE_IMPLEMENTATION
#define STB_IMAGE_RESIZE_IMPLEMENTATION
#include "stb_image_resize.h"
#endif

blk_imread::blk_imread() : blk("blk_imread") {
}

void blk_imread::process_one_task(const blk_payload in_payload) {
  if(d_debug) {
    std::cout << blk_name() << "::process_one_task() " << in_payload.d_string << std::endl;
  }

  blk_payload out_payload;
  out_payload.d_string = in_payload.d_string;
  out_payload.m_string["src_path"] = in_payload.m_string.at("src_path");

  int width, height, nchannel;
  std::string fn = in_payload.m_string.at("src_path") + in_payload.d_string;
  unsigned char *image_data = stbi_load(fn.c_str(), &width, &height, &nchannel, 0);
  if(image_data == NULL) {
    skip_one_task(in_payload, "failed to load image");
    return;
  }

  std::size_t npixel = width * height * nchannel;
  std::vector<uint8_t> rgb;
  if(nchannel == 1) {
    // create RGB from grayscale image
    rgb.resize(width * height * 3);
    std::size_t offset;
    for(std::size_t i=0; i<npixel; ++i) {
      offset = i*3;
      rgb[offset    ] = image_data[i];
      rgb[offset + 1] = image_data[i];
      rgb[offset + 2] = image_data[i];
    }
  } else {
    if(nchannel == 3) {
      // copy rgb image as it is
      rgb.resize(npixel);
      rgb.assign(image_data, image_data + npixel);
    } else {
      std::string reason = "image contains " + std::to_string(nchannel) + " channels -- only grayscale and RGB images accepted.";
      skip_one_task(in_payload, reason);
      stbi_image_free(image_data);
      return;
    }
  }
  stbi_image_free(image_data);

  out_payload.m_uint32["original_width"] = width;
  out_payload.m_uint32["original_height"] = height;
  out_payload.m_uint32["original_nchannel"] = nchannel;
  out_payload.m_string["dtype"] = d_conf_string.at("wanted_dtype");

  bool success = false;
  if(d_conf_string.count("resize_using") == 0 ||
     d_conf_string.at("resize_using") == "stb_imresize") {
    success = stb_imresize(rgb.data(), width, height, nchannel,
                           d_conf_uint32.at("wanted_width"),
                           d_conf_uint32.at("wanted_height"),
                           d_conf_uint32.at("wanted_nchannel"),
                           out_payload);
  } else {
    success = tflite_imresize(rgb.data(), width, height, nchannel,
                              d_conf_uint32.at("wanted_width"),
                              d_conf_uint32.at("wanted_height"),
                              d_conf_uint32.at("wanted_nchannel"),
                              out_payload);
  }

  if(success) {
    add_one_task(out_payload);
  } else {
    std::cerr << in_payload.d_string << ": failed to resize image";
  }
}


bool blk_imread::stb_imresize(unsigned char *rgb,
                          int image_width, int image_height, int image_channels,
                          int wanted_width, int wanted_height, int wanted_channels,
                          blk_payload &out_payload) {
  std::size_t output_number_of_pixels = wanted_height * wanted_width * wanted_channels;
  out_payload.v_uint8.resize(output_number_of_pixels);
  int result = stbir__resize_arbitrary(NULL,
                                       rgb, image_width, image_height, 0,
                                       out_payload.v_uint8.data(), wanted_width, wanted_height, 0,
                                       0,0,1,1,NULL,
                                       wanted_channels, -1, 0, STBIR_TYPE_UINT8,
                                       STBIR_FILTER_CUBICBSPLINE, STBIR_FILTER_CUBICBSPLINE,
                                       STBIR_EDGE_REFLECT, STBIR_EDGE_REFLECT, STBIR_COLORSPACE_SRGB);

  if(result == 0) {
    return false;
  }

  if(d_conf_string.count("wanted_dtype") == 1 &&
     d_conf_string.at("wanted_dtype") == "float") {
    out_payload.v_float.resize(output_number_of_pixels);
    if(d_conf_bool.at("element_value_scale") == true) {
      // normalize to range of [element_value_min, element_value_max]
      float scale_factor = (d_conf_float["element_value_max"] - d_conf_float["element_value_min"]) / 255.0F;
      for (int i = 0; i < output_number_of_pixels; i++) {
        out_payload.v_float[i] = (((float) out_payload.v_uint8[i]) * scale_factor) + d_conf_float["element_value_min"];
      }
    } else {
      float scale = 1.0f / 255.0f;
      for (int i = 0; i < output_number_of_pixels; i++) {
        float val = ((float) out_payload.v_uint8[i]) * scale;
        out_payload.v_float[i] = fmin( val, 1.0f );
      }
    }
    out_payload.v_uint8.clear();
  }

  out_payload.m_uint32["width"] = wanted_width;
  out_payload.m_uint32["height"] = wanted_height;
  out_payload.m_uint32["nchannel"] = wanted_channels;
  return true;
}

// source: https://github.com/tensorflow/tensorflow/blob/master/tensorflow/lite/examples/label_image/bitmap_helpers_impl.h
bool blk_imread::tflite_imresize(unsigned char *rgb,
                          int image_width, int image_height, int image_channels,
                          int wanted_width, int wanted_height, int wanted_channels,
                          blk_payload &out_payload) {
  int number_of_pixels = image_height * image_width * image_channels;
  std::unique_ptr<tflite::Interpreter> interpreter(new tflite::Interpreter);

  int base_index = 0;

  // two inputs: input and new_sizes
  interpreter->AddTensors(2, &base_index);
  // one output
  interpreter->AddTensors(1, &base_index);
  // set input and output tensors
  interpreter->SetInputs({0, 1});
  interpreter->SetOutputs({2});

  // set parameters of tensors
  TfLiteQuantizationParams quant;
  interpreter->SetTensorParametersReadWrite(
                                            0, kTfLiteFloat32, "input",
                                            {1, image_height, image_width, image_channels}, quant);
  interpreter->SetTensorParametersReadWrite(1, kTfLiteInt32, "new_size", {2},
                                            quant);
  interpreter->SetTensorParametersReadWrite(
                                            2, kTfLiteFloat32, "output",
                                            {1, wanted_height, wanted_width, wanted_channels}, quant);

  tflite::ops::builtin::BuiltinOpResolver resolver;
  const TfLiteRegistration* resize_op =
    resolver.FindOp(tflite::BuiltinOperator_RESIZE_BILINEAR, 1);
  auto* params = reinterpret_cast<TfLiteResizeBilinearParams*>(malloc(sizeof(TfLiteResizeBilinearParams)));
  params->align_corners = false;
  //params->half_pixel_centers = false;
  interpreter->AddNodeWithParameters({0, 1}, {2}, nullptr, 0, params, resize_op,
                                     nullptr);

  interpreter->AllocateTensors();

  auto input = interpreter->typed_tensor<float>(0);
  for (int i = 0; i < number_of_pixels; i++) {
    input[i] = rgb[i];
  }

  // fill new_sizes
  interpreter->typed_tensor<int>(1)[0] = wanted_height;
  interpreter->typed_tensor<int>(1)[1] = wanted_width;

  interpreter->Invoke();

  auto output = interpreter->typed_tensor<float>(2);
  auto output_number_of_pixels = wanted_height * wanted_width * wanted_channels;

  if(d_conf_string.count("wanted_dtype") == 1 &&
     d_conf_string.at("wanted_dtype") == "float") {
    out_payload.v_float.resize(output_number_of_pixels);
    for (int i = 0; i < output_number_of_pixels; i++) {
      out_payload.v_float[i] = output[i] / 255.0;
    }
  } else {
    out_payload.v_uint8.resize(output_number_of_pixels);
    for (int i = 0; i < output_number_of_pixels; i++) {
      out_payload.v_uint8[i] = static_cast<uint8_t>(output[i]);
    }
  }
  out_payload.m_uint32["width"] = wanted_width;
  out_payload.m_uint32["height"] = wanted_height;
  out_payload.m_uint32["nchannel"] = wanted_channels;
  return true;
}

void blk_imread::skip_one_task(const blk_payload &in_payload, std::string reason) const {
  std::clog << "\nSKIPPED " << in_payload.d_string << ": "
            << reason << std::endl;
}
