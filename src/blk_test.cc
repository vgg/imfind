#include "blk_flist.h"
#include "blk_imread.h"
#include "blk_null_sink.h"
#include "blk_imwrite.h"
#include "blk_imfeat.h"
#include "blk_feat_index.h"

#include <iostream>
#include <thread>
#include <chrono>
#include <algorithm>
#include <limits>

int main(int argc, char** argv) {
  if(argc != 2) {
    std::cout << "Usage: " << argv[0] << " PATH" << std::endl;
    return 0;
  }

  blk_flist flist;
  flist.output_buffer_size(std::numeric_limits<uint32_t>::max());
  flist.src_path(argv[1]);

  blk_imread imread;
  imread.output_buffer_size(999);
  imread.d_conf_uint32["wanted_width"] = 224;
  imread.d_conf_uint32["wanted_height"] = 224;
  imread.d_conf_uint32["wanted_nchannel"] = 3;
  //imread.d_conf_string["wanted_dtype"] = "uint8";
  imread.d_conf_string["wanted_dtype"] = "float";

  blk_imfeat imfeat;
  imfeat.output_buffer_size(999);
  /*
  imfeat.d_conf_string["tflite_model_fn"] = "/home/tlm/.imfind/asset/efficientnet/efficientnet_lite4_fp32.tflite";
  imfeat.d_conf_uint32["tflite_model_input_index"] = 0;
  imfeat.d_conf_uint32["tflite_model_output_index"] = 295; // 10x10x272
  imfeat.d_conf_string["tflite_model_output_process"] = "AveragePool2D";
  */
  imfeat.d_conf_string["tflite_model_fn"] = "/home/tlm/.imfind/asset/efficientnet/efficientnet_lite0_fp32.tflite";
  imfeat.d_conf_uint32["tflite_model_input_index"] = 0;
  imfeat.d_conf_uint32["tflite_model_output_index"] = 155; // 7x7x192
  imfeat.d_conf_string["tflite_model_output_process"] = "AveragePool2D";

  int max_threads = std::thread::hardware_concurrency() - 4;
  imfeat.d_conf_uint32["tflite_interpreter_num_threads"] = std::max(1, max_threads);
  imfeat.get_ready_for_inference();

  blk_feat_index feat_index;
  feat_index.d_conf_uint32["estimated_file_count"] = flist.file_count();
  feat_index.d_conf_uint32["feature_dimension"] = 192;
  feat_index.d_conf_float["index_search_radius"] = 1e-3;
  feat_index.get_ready_for_indexing();

  blk_null_sink null_sink;
  null_sink.output_buffer_size(999);

  blk_imwrite imwrite;
  imwrite.d_conf_string["dst_path"] = "/home/tlm/data/imfind/blk/resized/";
  //imwrite.print_flow_graph();
  //std::this_thread::sleep_for(std::chrono::seconds(1));

  // connect all blocks
  imread.in(&flist);
  //imwrite.in(&imread);
  imfeat.in(&imread);
  feat_index.in(&imfeat, true); // wait until finish
  //null_sink.in(&imfeat, true); // wait until null_sink is done
  //null_sink.print_flow_graph();

  std::cout << "Exiting ..." << std::endl;
  return 0;
}
