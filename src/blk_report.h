#ifndef BLK_REPORT_H
#define BLK_REPORT_H

// generates a report (e.g. HTML, CSV, etc.) from match results
//
// Author: Abhishek Dutta <adutta@robots.ox.ac.uk>
// Date: 29 April 2020

#include "blk_payload.h"
#include "blk.h"

#include <iostream>
#include <sstream>
#include <map>
#include <string>
#include <fstream>

class blk_report : public blk {
public:
  blk_report();

private:
  void process_one_task(const blk_payload in_payload);
  void group_all_matches();
  void write_group_match_to_html();
  void write_group_match_to_csv();
  void on_all_done();
  void show_progress_in_console();
  void get_all_other_matches(uint32_t findex,
                             std::vector<uint32_t> &group_findex_list,
                             std::vector<bool> &visited_flag);

  std::ostringstream d_html;
  std::vector<std::string> d_filename;
  std::unordered_map<uint32_t, std::vector<uint32_t> > d_match;
  std::map<uint32_t, std::vector<uint32_t>, std::greater<uint32_t> > d_match_count;
  std::vector<std::vector<uint32_t> > d_grouped_match;

  bool d_all_done;
};

#endif
