/*
** util : utility functions used by imfind and its modules
**
** Author: Abhishek Dutta <adutta _at_ robots.ox.ac.uk>
** Date: Dec. 15, 2019
**
*/

#ifndef IMFIND_UTIL_H
#define IMFIND_UTIL_H

#include <map>
#include <string>
#include <vector>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <dirent.h>
#include <regex>
#include <ctime>
#include <fstream>
#include <unordered_map>

namespace imfind {
  void parse_cmdline_args(const int argc, const char **argv,
                          std::map<std::string, std::string> &cmdline_args);
  void print_cmdline_args(std::map<std::string, std::string> const &cmdline_args);

  bool fs_dir_exists(std::string p);
  bool fs_is_dir(std::string p);
  bool fs_file_exists(std::string p);
  bool fs_mkdir(std::string p);
  bool fs_mkdir_if_not_exists(std::string p);
  void fs_list_img_files(std::string target_dir,
                         std::vector<std::string> &fn_list,
                         std::string filename_prefix="");

  bool cache_create(const std::string target_dir);
  std::string cache_target2dir(const std::string target_dir);
  bool cache_remove(const std::string target_dir);
  bool cache_clear_all();
  bool cache_clear_all_subfolders(std::string p);

  std::string testdir_create(const std::string test_name);
  bool testdir_remove(const std::string test_name);

  std::string imfind_homedir();
  std::string test_homedir();
  std::string cache_homedir();
  std::string asset_homedir();
  bool init_all_homedir();

  void split(const std::string &s,
             const char separator,
             std::vector<std::string> &chunks);

  void load_fn_list(const std::string fn,
                    std::vector<std::string> &fnlist);
  const std::unordered_map<std::string, uint8_t> IMFEAT_DATA_TYPE = { {"uint8_t", 1}, {"float32", 2} };

  void read_imfeat(const std::string fn,
                   uint8_t data_type_id, // [1:uint8_t, ...]
                   uint32_t feat_dim,
                   std::vector<uint8_t> &imfeat);

  std::string now_timestamp();
  bool debug_is_fn_prefix_same(std::string f1, std::string f2);
  uint32_t debug_count_fn_copy(std::string fn, std::vector<std::string> fnlist);

  void print_imhist(const std::vector<uint8_t> &rgb,
                    const std::string name);
}
#endif
