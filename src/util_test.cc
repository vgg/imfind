#include "util.h"

#include <fstream>

int test_parse_cmdline_args() {
  std::map<std::string, std::string> cmdline_args;

  // Test case 1
  const char* test_argv1[] = { "imfind", "--like-file", "query.jpg", "--region", "2,11,800,600", "--export-html", "/data/ok5k", NULL };
  int test_argc1 = ( sizeof(test_argv1)/sizeof(test_argv1[0]) )- 1;
  imfind::parse_cmdline_args(test_argc1, test_argv1, cmdline_args);
  if( cmdline_args.count("--export-html") != 1 ||
      cmdline_args.count("--like-file") != 1 ||
      cmdline_args.count("--region") != 1 ) {
    return EXIT_FAILURE;
  }
  if( cmdline_args.at("--export-html") != "" ||
      cmdline_args.at("--like-file") != "query.jpg" ||
      cmdline_args.at("--region") != "2,11,800,600" ) {
    return EXIT_FAILURE;
  }

  // Test case 2
  const char* test_argv2[] = { "imfind", "--is-copy", "/data/imagenet", NULL };
  int test_argc2 = ( sizeof(test_argv2)/sizeof(test_argv2[0]) )- 1;
  imfind::parse_cmdline_args(test_argc2, test_argv2, cmdline_args);
  if( cmdline_args.count("--is-copy") != 1 ) {
    return EXIT_FAILURE;
  }
  if( cmdline_args.at("--is-copy") != "" ) {
    return EXIT_FAILURE;
  }

  // Test case 3
  const char* test_argv3[] = { "imfind", "--has-txt", "--like", "'london'", "/data/bbcnews", NULL };
  int test_argc3 = ( sizeof(test_argv3)/sizeof(test_argv3[0]) )- 1;
  imfind::parse_cmdline_args(test_argc3, test_argv3, cmdline_args);
  if( cmdline_args.count("--has-txt") != 1 ||
      cmdline_args.count("--like") != 1 ) {
    return EXIT_FAILURE;
  }
  if( cmdline_args.at("--has-txt") != "" ||
      cmdline_args.at("--like") != "'london'" ) {
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}

int test_cache() {
  if(!imfind::init_all_homedir()) {
    std::cout << "failed to initialize home directories"
              << std::endl;
    return EXIT_FAILURE;
  }

  std::string cache_target1("/home/tlm/_tmp");
  std::string cache_target2("/data/dataset/imagenet");
  imfind::cache_create(cache_target1);
  imfind::cache_create(cache_target2);

  std::string cache1_dir = imfind::cache_target2dir(cache_target1);
  std::string cache2_dir = imfind::cache_target2dir(cache_target2);
  std::string tmp_fn(cache2_dir + "test_data_file.dat");
  std::ofstream f(tmp_fn);
  f << "test data";
  f.close();

  if(!imfind::fs_file_exists(tmp_fn)) {
    return EXIT_FAILURE;
  }

  imfind::cache_remove(cache_target1);
  imfind::cache_remove(cache_target2);

  if(imfind::fs_file_exists(tmp_fn)) {
    return EXIT_FAILURE;
  }
  // Note: folders may exist if they contain non-empty subfolders
  return EXIT_SUCCESS;
}

int main(int argc, char **argv) {
  int test_result;
  test_result = test_cache();
  if(test_result == EXIT_FAILURE) {
    return EXIT_FAILURE;
  }

  test_result = test_parse_cmdline_args();
  if(test_result == EXIT_FAILURE) {
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
