# imfind

imfind is a command line tool for visual search of images. It processes a
directory containing images and finds out all the images matching visual
content defined by the user.

For more details, see the output of `imfind --help` command.

## Compiling
Download the `imfind` source code and extract it in a folder called `$IMFIND_SRC` (e.g. `$IMFIND_SRC=/home/tlm/imfind` )

```
$ export IMFIND_REPO=~/imfind_repo && mkdir -p $IMFIND_REPO && cd $IMFIND_REPO
$ git clone git@gitlab.com:vgg/imfind.git
$ cd imfind && mkdir cmake_build && cd cmake_build
$ cmake ../src && make -j8
$ ./imfind --is-same --export-html ~/.imfind/test/is-same/
$ ./imfind --help
```

## Usage
Exact duplicates (i.e. same pixelwise) in a folder `/dataset/ashmolean` can be
found using:
```
./imfind --is-same --export-html /dataset/ashmolean
```

This will automatically open a web browser and load an offline HTML page that
shows all the duplicate images.

## Citation
If you use this software, please cite it as follows:
```
@misc{dutta2020imfind,
  author = "Dutta, A. and Zissermann, A.",
  title = "{imfind} : a command line tool for visual search of images",
  year = "2020",
  howpublished = "http://www.robots.ox.ac.uk/~vgg/software/imfind/",
  note = "Version: X.Y.Z, Accessed: INSERT_DATE_HERE"
}

```

## Contact
Contact [Abhishek Dutta](adutta_REMOVE_@robots.ox.ac.uk) for any queries or
feedback related to this software.

## Acknowledgements
This work is supported by EPSRC programme grant Seebibyte: Visual Search for the Era of Big Data ( [EP/M013774/1](http://www.seebibyte.org/index.html) )