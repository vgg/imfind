# convert face annotations from vggface2 dataset to coco format
#
# Author: Abhishek Dutta <adutta@robots.ox.ac.uk>
# Date: 5 May 2020

import csv
import os
import numpy as np
import random
import math
from PIL import Image
import json

## VGG Face 2
faceimg_fnlist_fn = '/home/tlm/dataset/vggface2/data/meta_infor/test_list.txt'
faceimg_dir = '/home/tlm/dataset/vggface2/meta_backup/vggface2_li/'
faceimg_metadata_fn = '/home/tlm/dataset/vggface2/whole_meta/meta_csv/test_1.csv'
coco_json_outdir = '/home/tlm/data/imfind/efficientdet/vggface2/coco/'

## read face image metadata
vggface2 = {}
pid_list = []
with open(faceimg_metadata_fn, 'r') as f:
    reader = csv.reader(f, quotechar='"', delimiter=',')
    next(reader) # skip first line
    for row in reader:
        pid, fid_and_face_count = row[0].split('/')
        fid, faceid = fid_and_face_count.split('_') # e.g. 0352_02
        imfn = os.path.join(faceimg_dir, row[0] + '.jpg')
        x = int(row[2])
        y = int(row[3])
        w = int(row[4])
        h = int(row[5])
        if pid not in vggface2:
            vggface2[pid] = {}
            pid_list.append(pid)
        if fid not in vggface2[pid]:
            vggface2[pid][fid] = []
        vggface2[pid][fid].append({'faceid':faceid, 'region':[x, y, w, h]})

random.seed(a=9973)
random.shuffle(pid_list)
pid_list_index = np.arange(len(pid_list))

dset_split_factor_list = [0.0, 0.6, 0.8, 1.0]
dset_name_list = ['train', 'valid', 'test']
for i in range(len(dset_split_factor_list) - 1):
    pid_list_start_index = math.floor(dset_split_factor_list[i] * len(pid_list))
    pid_list_end_index = math.floor(dset_split_factor_list[i+1] * len(pid_list))
    dset_name = dset_name_list[i]
    image_id = 1;
    annotation_id = 1;
    dset = {
        'info': {
            'year':'2020',
            'version':dset_name,
            'description':'Face detection dataset extracted from [whole_meta/meta_csv/test_1.csv] in VGG Face 2',
            'contributor':'',
            'url':'http://www.robots.ox.ac.uk/~vgg/data/vgg_face2/',
            'date_created':'07 May 2020',
        },
        'images': [],
        'annotations':[],
        'licenses':[],
        'categories':[ { 'id':1, 'name':'human_face', 'super_category':'human_face' }, ],
    }

    print('%s: from %d to %d' % (dset_name, pid_list_start_index, pid_list_end_index))
    for pid_list_index in range(pid_list_start_index, pid_list_end_index, 1):
        pid = pid_list[pid_list_index]
        for fid in vggface2[pid]:
            imfullfn = os.path.join(faceimg_dir, pid, fid + '.jpg')
            imfn = pid + '/' + fid + '.jpg'
            with Image.open(imfullfn) as im:
                imwidth, imheight = im.size
            image = { 'id':image_id,
                      'width':imwidth,
                      'height':imheight,
                      'file_name':imfn,
                      'license':'CC',
                      'flickr_url':'',
                      'coco_url':'',
                      'date_captured':''
            }
            dset['images'].append(image)
            for facei in vggface2[pid][fid]:
                x, y, w, h = facei['region']
                if x < 0:
                    x = 0
                if y < 0:
                    y = 0
                if (x+w) > imwidth:
                    w = imwidth - x

                if (y+h) > imheight:
                    h = imheight - y

                annotation = { 'id':annotation_id,
                           'image_id':image_id,
                           'category_id':1,
                           'segmentation':[[x, y, x+w, y, x+w, y+h, x, y+h]],
                           'area': (w*h)/ (imwidth*imheight),
                           'bbox':[x, y, w, h],
                           'iscrowd':0
                }
                dset['annotations'].append(annotation)
                annotation_id = annotation_id + 1
            image_id = image_id + 1

    dset_coco_fn = os.path.join(coco_json_outdir, dset_name + '.json')
    with open(dset_coco_fn, 'w') as f:
        json.dump(dset, f, )
        print('Written %s' % (dset_coco_fn))
