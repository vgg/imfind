# convert face annotations from wider face dataset to coco format
#
# Author: Abhishek Dutta <adutta@robots.ox.ac.uk>
# Date: 14 May 2020

import csv
import os
import numpy as np
import random
import math
from PIL import Image
import json
import argparse
import sys

from enum import Enum
class state(Enum):
    EXPECT_FILENAME = 1
    EXPECT_FACE_COUNT = 2
    EXPECT_DETECTION = 3

def check_widerface_dir(widerdir):
    if( os.path.exists(os.path.join(widerdir, 'wider_face_split')) and
        os.path.exists(os.path.join(widerdir, 'wider_face_split', 'wider_face_train_bbx_gt.txt')) and
        os.path.exists(os.path.join(widerdir, 'wider_face_split', 'wider_face_val_bbx_gt.txt')) and
        os.path.exists(os.path.join(widerdir, 'WIDER_train', 'images')) and
        os.path.exists(os.path.join(widerdir, 'WIDER_val', 'images')) ):
        return True
    else:
        return False

def parse_wider_metadata(gt_fn):
    dset = {}
    with open(gt_fn, 'r') as f:
        current_state = state.EXPECT_FILENAME
        fn = ''
        face_count = 0
        remaining_detection = 0
        for di in f:
            line = di
            if di.endswith('\n'):
                line = di[0:len(di)-1]
            #print('%s: LINE={%s}' % (current_state.name, line))

            if current_state is state.EXPECT_FILENAME:
                fn = line
                if fn not in dset:
                    dset[fn] = []
                    current_state = state.EXPECT_FACE_COUNT
                else:
                    print('Unexpected scenario')
                continue
            if current_state is state.EXPECT_FACE_COUNT:
                face_count = int(line)
                remaining_detection = face_count
                current_state = state.EXPECT_DETECTION
                continue
            if current_state is state.EXPECT_DETECTION:
                if remaining_detection == 0: # indicates 0 detections
                    current_state = state.EXPECT_FILENAME # initialize next state
                    continue # discard "0 0 0 0 0 0 0" entry and move to next line
                
                if remaining_detection == 1: # indicates last detection
                    current_state = state.EXPECT_FILENAME # initialize next state

                remaining_detection = remaining_detection - 1
                detection = line.split(' ')
                if len(detection) < 4:
                    #print('  Discarded malformed bbox: %s: %s' % (fn, line))
                    continue
                if len(detection) > 8:
                    if int(detection[7]) == 1:
                        #print('  Discarded invalid detection: %s: %s' % (fn, line))
                        continue # discard invalid image

                    x = int(detection[0])
                    y = int(detection[1])
                    w = int(detection[2])
                    h = int(detection[3])
                    dset[fn].append( { 'bbox':[x,y,w,h] } )
                    continue
            print('********* Unknown state: %s' % (current_state.name))
    return dset

def widerface_to_coco(widerface_dir, dset_name):
    gt_bb_fn = os.path.join(widerface_dir, 'wider_face_split', 'wider_face_' + dset_name + '_bbx_gt.txt')
    print('Processing %s' % (gt_bb_fn))
    widerface_dset = parse_wider_metadata(gt_bb_fn)
    image_id = 1;
    annotation_id = 1;
    dset = {
        'info': {
            'year':'2015',
            'version':dset_name,
            'description':'WIDER FACE dataset is a face detection benchmark dataset',
            'contributor':'',
            'url':'http://shuoyang1213.me/WIDERFACE/',
            'date_created':'',
        },
        'images': [],
        'annotations':[],
        'licenses':[],
        'categories':[ { 'id':1, 'name':'human_face', 'super_category':'human_face' }, ],
    }

    for imfn in widerface_dset:
        imfullfn = os.path.join(widerface_dir, 'WIDER_' + dset_name, 'images', imfn)
        with Image.open(imfullfn) as im:
            imwidth, imheight = im.size
            image = { 'id':image_id,
                      'width':imwidth,
                      'height':imheight,
                      'file_name':imfn,
                      'license':'',
                      'flickr_url':'',
                      'coco_url':'',
                      'date_captured':''
            }
            dset['images'].append(image)
            for facei in widerface_dset[imfn]:
                x, y, w, h = facei['bbox']
                if x < 0:
                    x = 0
                if y < 0:
                    y = 0
                if (x+w) > imwidth:
                    w = imwidth - x
                if (y+h) > imheight:
                    h = imheight - y

                annotation = { 'id':annotation_id,
                               'image_id':image_id,
                               'category_id':1,
                               'segmentation':[[x, y, x+w, y, x+w, y+h, x, y+h]],
                               'area': w*h,
                               'bbox':[x, y, w, h],
                               'iscrowd':0
                }
                dset['annotations'].append(annotation)
                annotation_id = annotation_id + 1
            image_id = image_id + 1

    print('  extracted %d annotations from %d images' % (annotation_id - 1, image_id -1))
    dset_coco_fn = os.path.join(widerface_dir, 'wider_face_split', 'wider_face_' + dset_name + '_bbx_gt_coco.json')
    with open(dset_coco_fn, 'w') as f:
        json.dump(dset, f, )
        print('  Written %s' % (dset_coco_fn))


if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Convert WIDER Face annotations to COCO format")
    parser.add_argument("--widerface_dir",
                        required=True,
                        type=str,
                        help="location of folder which contains the WIDER Face dataset")
    args = parser.parse_args()
    if not check_widerface_dir(args.widerface_dir):
        print('''WIDERFACE_DIR should contain the following items
wider_face_split
        |- wider_face_train_bbx_gt.txt
        |- wider_face_val_bbx_gt.txt
WIDER_train
        |- images
WIDER_val
        |- images''')
        sys.exit()
    widerface_to_coco(args.widerface_dir, 'train')
    widerface_to_coco(args.widerface_dir, 'val')