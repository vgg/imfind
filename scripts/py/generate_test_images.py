#
# Generate test dataset for validation of imfind
#
# Author: Abhishek Dutta <adutta @ robots.ox.ac.uk>
# Date: 18 Jan. 2020
#
import os
from PIL import Image
import numpy as np
import shutil
import imgaug.augmenters as iaa

SRC='/home/tlm/data/imfind/test/original/'
DISTRACTOR_SRC='/home/tlm/data/imfind/test/distractor/original/'

TGT_BASEDIR='/home/tlm/data/imfind/test/testset/'
TGT_WIDTH = 256
TGT_HEIGHT = 256
SCALE_PERCENT = [0.2, 0.6]
CROP_SIZE = [1, 20]
MAX_FILES = 25

TGTBASE = os.path.join(TGT_BASEDIR, 'dim%dx%d_scale%.1fto%.1f_crop%dto%d' % (TGT_WIDTH,TGT_HEIGHT, SCALE_PERCENT[0], SCALE_PERCENT[1], CROP_SIZE[0], CROP_SIZE[1]))

if os.path.exists(TGTBASE):
    shutil.rmtree(TGTBASE)
os.makedirs(TGTBASE)

## load all images and assign it a unique-id
## file-id range is from 0000.jpg to 9999.jpg
fnlist = list()
originalimglist = list()
resizedimglist = list()
fidlist = list()
fid = 1

for fn in os.listdir(SRC):
    fidstr = '%.3d' % (fid)
    fpath = os.path.join(SRC, fn)
    fidlist.append(fidstr)
    fnlist.append(fpath)
    img = Image.open(fpath)
    img.load()
    img = img.convert('RGB')
    width, height = img.size[:2]
    originalimglist.append(np.asarray(img, dtype='uint8'))

    if height > width:
        hpercent = (TGT_HEIGHT/float(img.size[1]))
        wsize = int((float(img.size[0])*float(hpercent)))
        img = img.resize((wsize, TGT_HEIGHT), Image.ANTIALIAS)
    else:
        wpercent = (TGT_WIDTH/float(img.size[0]))
        hsize = int((float(img.size[1])*float(wpercent)))
        img = img.resize((TGT_WIDTH,hsize), Image.ANTIALIAS)

    resizedimglist.append(np.asarray(img, dtype='uint8'))
    fid = fid + 1
    if MAX_FILES != -1 and fid > MAX_FILES:
        break

## load all distractor images and assign it a unique-id
## file-id range is from distractor-0000.jpg to distractor-9999.jpg
distractor_fnlist = list()
distractor_originalimglist = list()
distractor_resizedimglist = list()
distractor_fidlist = list()
distractor_fid = 1

for fn in os.listdir(DISTRACTOR_SRC):
    fidstr = 'distractor-%.3d' % (distractor_fid)
    fpath = os.path.join(DISTRACTOR_SRC, fn)
    distractor_fidlist.append(fidstr)
    distractor_fnlist.append(fpath)
    img = Image.open(fpath)
    img.load()
    img = img.convert('RGB')
    width, height = img.size[:2]
    distractor_originalimglist.append(np.asarray(img, dtype='uint8'))

    if height > width:
        hpercent = (TGT_HEIGHT/float(img.size[1]))
        wsize = int((float(img.size[0])*float(hpercent)))
        img = img.resize((wsize, TGT_HEIGHT), Image.ANTIALIAS)
    else:
        wpercent = (TGT_WIDTH/float(img.size[0]))
        hsize = int((float(img.size[1])*float(wpercent)))
        img = img.resize((TGT_WIDTH,hsize), Image.ANTIALIAS)

    distractor_resizedimglist.append(np.asarray(img, dtype='uint8'))
    distractor_fid = distractor_fid + 1
    if MAX_FILES != -1 and distractor_fid > MAX_FILES:
        break

## create a set of transformations
txlist = {}
txlist['same'] = [
    {'name':'equal',
     'src':resizedimglist,
     'distractor_src':distractor_resizedimglist,
     'iaa_seq':iaa.Sequential()
    },
]

txlist['copy'] = [
    {'name':'resize',
     'src':originalimglist,
     'distractor_src':distractor_originalimglist,
     'iaa_seq':iaa.Sequential([iaa.Resize((SCALE_PERCENT[0], SCALE_PERCENT[1]))])
    },
    {'name':'resize-gray',
     'src':originalimglist,
     'distractor_src':distractor_originalimglist,
     'iaa_seq':iaa.Sequential([iaa.Resize((SCALE_PERCENT[0], SCALE_PERCENT[1])),
                               iaa.Grayscale(alpha=1.0)], random_order=False)
    },
    {'name':'gray',
     'src':resizedimglist,
     'distractor_src':distractor_resizedimglist,
     'iaa_seq':iaa.Sequential([iaa.Grayscale(alpha=1.0)])
    },
    {'name':'resize-hflip',
     'src':originalimglist,
     'distractor_src':distractor_originalimglist,
     'iaa_seq':iaa.Sequential([iaa.Resize((SCALE_PERCENT[0], SCALE_PERCENT[1])),
                               iaa.HorizontalFlip(1.0)], random_order=False),
    },
    {'name':'hflip',
     'src':resizedimglist,
     'distractor_src':distractor_resizedimglist,
     'iaa_seq':iaa.Sequential([iaa.HorizontalFlip(1.0)], random_order=False)
    },
    {'name':'resize-vflip',
     'src':originalimglist,
     'distractor_src':distractor_originalimglist,
     'iaa_seq':iaa.Sequential([iaa.Resize((SCALE_PERCENT[0], SCALE_PERCENT[1])),
                               iaa.VerticalFlip(1.0)], random_order=False)
    },
    {'name':'vflip',
     'src':resizedimglist,
     'distractor_src':distractor_resizedimglist,
     'iaa_seq':iaa.Sequential([iaa.VerticalFlip(1.0)], random_order=False)
    },
    {'name':'rot90n',
     'src':originalimglist,
     'distractor_src':distractor_originalimglist,
     'iaa_seq':iaa.Sequential([iaa.Resize((SCALE_PERCENT[0], SCALE_PERCENT[1])),
                               iaa.Rot90((1, 3), keep_size=False)], random_order=False)
    },
]

txlist['topy'] = [
    {'name':'resize-crop',
     'src':originalimglist,
     'distractor_src':distractor_originalimglist,
     'iaa_seq':iaa.Sequential([ iaa.Resize((SCALE_PERCENT[0], SCALE_PERCENT[1])),
                                iaa.Crop(px=(CROP_SIZE[0], CROP_SIZE[1]),
                                         sample_independently=True,
                                         keep_size=False
                                )], random_order=False)
    },
    {'name':'crop',
     'src':resizedimglist,
     'distractor_src':distractor_resizedimglist,
     'iaa_seq':iaa.Sequential([ iaa.Crop(px=(CROP_SIZE[0], CROP_SIZE[1]),
                                         sample_independently=True,
                                         keep_size=False
                                )], random_order=False)
    },
]

for txtype in txlist:
    TGT = os.path.join(TGTBASE, txtype)
    os.makedirs(TGT)

    print('Creating original images with size %d x %d' % (TGT_WIDTH, TGT_HEIGHT))

    ## copy original (resized)
    for findex in range(0, len(fnlist)):
        fid = fidlist[findex]
        fname = os.path.join(TGT, fid + '.jpg')
        fimg = Image.fromarray(resizedimglist[findex])
        fimg.save(fname)

    ## process requested transformation
    for tx in txlist[txtype]:
        name = tx['name']
        src = tx['src']
        iaa_seq = tx['iaa_seq']
        print('Processing %s' % (name))
        tximglist = iaa_seq(images=src)

        ## copy transformations
        for findex in range(0, len(fnlist)):
            fid = fidlist[findex]
            fname = os.path.join(TGT, fid + '-' + txtype + '-' + name + '.jpg')
            fimg = Image.fromarray(tximglist[findex])
            fimg.save(fname)

        ## copy distrators
        distractor_src = tx['distractor_src']
        distractor_tximglist = iaa_seq(images=distractor_src)
        for distractor_findex in range(0, len(distractor_fnlist)):
            distractor_fid = distractor_fidlist[distractor_findex]
            distractor_fname = os.path.join(TGT, distractor_fid + '-' + txtype + '-' + name + '.jpg')
            distractor_fimg = Image.fromarray(distractor_tximglist[distractor_findex])
            distractor_fimg.save(distractor_fname)
